import React from 'react'
import "./SelectTyles.css"
import Brintania  from '../../assests/Brintania.jpg'
import Enchanted  from '../../assests/Enchanted.webp'
import Laswitan  from '../../assests/laswitan-laoon.webp'
import TinuyAnn  from '../../assests/Tinut-ann-falls.jpg'
import Hagonoy  from '../../assests/hagonoy.jpg'
import Kawakawa  from '../../assests/kawakawa.jpg'

import Selectimg from '../selectImg/Selectimg'


const Select = () => {
  return (
    <div name="views"  className='select-wrapper'>
        <div className='container'>
        <Selectimg bgImg={Enchanted} text='Enchanted'/>
        <Selectimg bgImg={Brintania} text='Brintania'/>
        <Selectimg bgImg={Laswitan} text='Laswitan'/>
        <Selectimg bgImg={TinuyAnn} text='TinuyAnn '/>
        <Selectimg bgImg={Hagonoy} text='Hagonoy'/>
        <Selectimg bgImg={Kawakawa} text='Kawakawa'/>
        </div>
        
    </div>
  )
}

export default Select
