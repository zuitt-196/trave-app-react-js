import React from 'react'
import './DistantionsStyles.css'
import Brintania  from '../../assests/Brintania.jpg'
import Enchanted  from '../../assests/Enchanted.webp'
import Laswitan  from '../../assests/laswitan-laoon.webp'
import TinuyAnn  from '../../assests/Tinut-ann-falls.jpg'
import Hagonoy  from '../../assests/hagonoy.jpg'
import Kawakawa  from '../../assests/kawakawa.jpg'

const Distantions = () => {
  return (
    <div name="desitanation" className="distanation-wrapper">
          <div className="container">
              <h1>All-inclusive Resorts</h1>
              <p>On the relaxing nature </p>
              <div className="distanation-image-container">
                  <img className='span-3 image-grid-row-2' src={Brintania} alt="/" />
                  <img src={Enchanted} alt="/" />
                  <img src={Laswitan} alt="/" />
                  <img src={TinuyAnn} alt="/" />
                  <img src={Hagonoy} alt="/" />
                  <img src={Kawakawa} alt="/" />
              </div>
          </div>
    </div>
  )
}

export default Distantions
