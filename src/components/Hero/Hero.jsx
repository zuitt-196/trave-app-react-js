  import React from 'react'
  import './HeroStyles.css'

  import Video from '../../assests/sea.mp4';
import{BsSearch } from 'react-icons/bs';

  const Hero = () => {
    return (
      <div className='hero-wrapper'>
          <video src={Video}  controls="controls" autoPlay  loop muted id="video"/>
          {/* OVERLAY BACKGROUND IN VIDEO */}
          <div className='overLay'></div>

          <div className='content'>
              <h1>First class Travel</h1>
              <h2>Top 1% Surigao location wordwide</h2>
              <form className='form'>
                  <div>
                    <input type="text" placeholder='Seach Destination'/>
                  </div>
                  <div> 
                     <button><BsSearch/></button>
                  </div>
              </form>
          </div>
        

      </div>
    )
  }

  export default Hero
