import React from 'react'
import './ImageCarouse.css'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel';

import Brintania  from '../../assests/Brintania.jpg'
import Enchanted  from '../../assests/Enchanted.webp'
import Laswitan  from '../../assests/laswitan-laoon.webp'
import TinuyAnn  from '../../assests/Tinut-ann-falls.jpg'
import Hagonoy  from '../../assests/hagonoy.jpg'
import Kawakawa  from '../../assests/kawakawa.jpg'

const ImageCarouse = () => {
  return (
    <div name="carousel" className='container'>
                <Carousel className='carousel' showArrows={true} autoPlay={true} infiniteLoop={true}>
                <div>
                    <img src={Brintania} />
                    {/* <p className="legend">Legend 1</p> */}
                </div>
                <div>
                    <img src={Enchanted} />
                    {/* <p className="legend">Legend 2</p> */}
                </div>
                <div>
                    <img src={Laswitan} />
                    {/* <p className="legend">Legend 3</p> */}
                </div>
</Carousel>
</div>
    )
}

export default ImageCarouse