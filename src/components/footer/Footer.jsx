import React from 'react';
import './Footer.css'
import {FaFacebook, FaInstagram, FaPinterest, FaTimes, FaTwitter, FaYoutube} from 'react-icons/fa'

const Footer = () => {
  return (
    <div className="footer-wrapper">
        <div className="container">
            <div className="top">
                <h3>BEACH</h3>
                <div className="socials">
                                    <FaFacebook className="icon"/>
                                    <FaInstagram className="icon"/>
                                    <FaTwitter className="icon"/>
                                    <FaPinterest className="icon"/>
                                    <FaYoutube className="icon"/>       
              </div>
        </div>
            <div className="bottom">
                <div className="left">
                    <ul>
                        <li>About</li>
                        <li>Partnerships</li>
                        <li>Carrerd</li>
                        <li>About</li>
                        <li>News roon</li>
                        <li>Advertsiment</li>
                    </ul>
                 </div>    

                 <div className="right">
                            <ul>
                                <li>Contacts</li>
                                <li>Terms</li>
                                <li>Privacy</li>
                                <li>Pricing</li>
                                
                            </ul>
                 </div>
            </div>   
        </div>
    </div>
  )
}

export default Footer
