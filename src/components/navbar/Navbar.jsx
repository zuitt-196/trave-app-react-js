import React,{useState} from 'react';
import './NavbarStyles.css';
import {BsSearch} from 'react-icons/bs'
import {BsFillPersonFill} from 'react-icons/bs'
import {AiOutlineMenu} from 'react-icons/ai'
import {FaFacebook, FaInstagram, FaPinterest, FaTimes, FaTwitter, FaYoutube} from 'react-icons/fa'

import { Link, Button, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

const Navbar = () => {
    // DEFINE THE STATE OF BOLLEAN
    const [nav, setNav] = useState(false);

    // DEFINE THE NAV MENU FUNCTION 
    const handleNav = () =>{
        setNav(!nav)
    }
  return (
    <div name="home" className={nav ? "navbar-wrapper navbar-bg" : "navbar-wrapper"}>
            <div className={nav ? "logo dark" : "logo"}>
                <h2>BEACHES.</h2>
            </div>
            <ul className="nav-menu">
                <Link to="home" smooth={true} duration={500}><li>Home</li></Link>
                <Link to="desitanation" smooth={true} duration={500}><li>Desitanation</li></Link>
                <Link to="carousel"  smooth={true} duration={500}><li>Travel</li></Link>
                <Link to="book"  smooth={true} duration={500}><li>Book</li></Link>
                <Link to="views"  smooth={true} duration={500}><li>Views</li></Link>
      
            </ul>
            <div className="nav-icons">
                <BsSearch className='icon'/>
                <BsFillPersonFill className='icon'/>
            </div>
                {/* MOBILE MENU SECTION */}
                <div className="hamburger" onClick={handleNav}>
                    {!nav ? (<AiOutlineMenu className='icon'/> ) : ( <FaTimes style={{color:"black"}} className='icon' />)}
                </div>

                <div className={nav ? "mobile-menu active" :"mobile-menu"}>
                        <ul className="mobile-nav">
                             <li>Home</li>
                             <li>Desitanation</li>
                             <li>Travel</li>
                             <li>Book</li>
                             <li>Views</li>
                        </ul>

                        <div className="mobile-menu-button">
                            <div className="menu-icons">
                                    <button>Seacrh</button>
                                    <button>Account</button>
                            </div>

                            <div className="social-icons">
                                    <FaFacebook className="icon"/>
                                    <FaInstagram className="icon"/>
                                    <FaTwitter className="icon"/>
                                    <FaPinterest className="icon"/>
                                    <FaYoutube className="icon"/>
                            </div>

                        </div>        
                </div>    
    </div>
  )
}

export default Navbar
