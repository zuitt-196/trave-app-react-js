import React from 'react'
import './selectimgStyle.css'



const Selectimg = ({text,bgImg}) => {
  return (
    <div className='selectImg-location'> 
        <img src={bgImg} alt="/" />
        <div className='overlay'>
          <p>{text}</p>
        </div>
    </div>
  )

}

export default Selectimg