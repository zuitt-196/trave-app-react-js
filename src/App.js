import ImageCarouse from "./components/couracel/ImageCarouse";
import Distantions from "./components/distanation/Distantions";
import Footer from "./components/footer/Footer";
import Hero from "./components/Hero/Hero";
import Navbar from "./components/navbar/Navbar";
import Search from "./components/search/Seacrh";
import Select from "./components/selects/Select";


function App() {
  return (
      <div>
        <Navbar/>
        <Hero/>
        <Distantions/>
        <Search/>
        <Select/>
        <ImageCarouse />
        <Footer/>
     </div>
  );
}

export default App;
